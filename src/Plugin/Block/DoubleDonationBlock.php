<?php

namespace Drupal\double_donation\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Cache;


/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "double_donation_block",
 *   admin_label = @Translation("Double the Donation Block"),
 *   category = @Translation("Double the Donation")
 * )
 */
class DoubleDonationBlock extends BlockBase implements ContainerFactoryPluginInterface{
  /**
    * @var \Drupal\Core\Config\ConfigFactory
    */
    protected $configFactory;

  /**
    * @var \GuzzleHttp\Client
    */
    private $client;

 /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('double_donation.block_cache')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactory $configFactory, CacheBackendInterface $cache_backend) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->cacheBackend = $cache_backend;

  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $block_config = $this->getConfiguration();
    $config = $this->configFactory->get('double_donation.settings');
    $api_key = $config->get('api_key');

    return [
      '#theme' => 'double_donation',
      '#api_key' => $api_key ?? null,
      '#cache' => [
        'contexts' => ['url.path']
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
  }

}
